## VAWebsite

VAWebsite is the project for the official static web presentation of VA (Virtual Acoustics).

You can browse it following the domains:

 - **[virtualacoustics.org](http://www.virtualacoustics.org) (official domain)**
 
 - [virtualacoustics.de](http://www.virtualacoustics.de) *(German domain, redirects to the official website)*

### Legal notice

The Institute of Technical Acoustics (ITA) at RWTH Aachen University is responsible for the contents of this website.


### License

Copyright 2015-2017 Institute of Technical Acoustics (ITA), RWTH Aachen University.

Creative Commons Attribution 4.0 Unported
http://creativecommons.org/licenses/by/4.0/


### Template

VAWebsite uses the following HTML template:

Landed by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)

#### Credits

Demo Images:
 - Unsplash (unsplash.com)

Icons:
 - Font Awesome (fortawesome.github.com/Font-Awesome)

Other:
 - jQuery (jquery.com)
 - html5shiv.js (@afarkas @jdalton @jon_neal @rem)
 - CSS3 Pie (css3pie.com)
 - Respond.js (j.mp/respondjs)
 - Skel (skel.io)